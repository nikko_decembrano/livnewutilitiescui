﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace LivNewCore.DAL
{
    public static class DataSource
    {
        public static SqlConnection ConnNetCRMAU;
        public static SqlConnection ConnLivExactAUS;
        public static SqlConnection ConnLivingData;
        public static SqlConnection ConnLivCRM;
        public static SqlConnection ConnWeb;
        public static SqlConnection ConnNetUniChoice;

        public static object GetValue(string sql, SqlConnection con)
        {
            //SqlConnection con = new SqlConnection();
            //con.ConnectionString = ConfigurationManager.ConnectionStrings[Sever];

            if (con.State == ConnectionState.Broken || con.State == ConnectionState.Closed) con.Open();

            SqlCommand com = con.CreateCommand();
            com.CommandType = CommandType.Text;
            com.CommandText = sql;
            com.CommandTimeout = 600000;

            object returnValue = null;

            try
            {
                //con.Open();
                // Hi Cathy, i deleted the .ToString() (com.ExecuteScalar().ToString())
                // because, there are situations that it returns null, and cannot call function ToString()
                // this function also can retrieve other datatyps, such as DateTime. 
                //
                // Comment by Bo
                ////DebuggerMethod(com);
                returnValue = com.ExecuteScalar();//.ToString();
            }
            catch (SqlException exSql)
            {
                if (exSql.Message.Contains("Timeout"))
                {
                    //MessageBox.Show("Sorry, the server is handling a lot of requests at the moment. \n Please try again in a few seconds.");
                }
                else
                {
                    //MessageBox.Show("SQL:" + exSql.ToString());
                }

            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }
            finally
            {
                //if (con.State == ConnectionState.Open) con.Close();
                com.Dispose();
                //con.Dispose();
            }
            return returnValue == null ? "" : returnValue;
        }

        public static object GetValue(string sql, SqlConnection con, object[,] parameters)
        {
            //SqlConnection con = new SqlConnection();
            //con.ConnectionString = ConfigurationManager.ConnectionStrings[Sever];

            if (con.State == ConnectionState.Broken || con.State == ConnectionState.Closed) con.Open();

            SqlCommand com = con.CreateCommand();
            com.CommandType = CommandType.Text;
            com.CommandText = sql;
            com.CommandTimeout = 600000;
            if (parameters != null)
                for (int i = 0; i < parameters.GetLength(0); i++)
                    com.Parameters.AddWithValue(parameters[i, 0].ToString(), parameters[i, 1]);
            object returnValue = null;

            try
            {
                //con.Open();
                // Hi Cathy, i deleted the .ToString() (com.ExecuteScalar().ToString())
                // because, there are situations that it returns null, and cannot call function ToString()
                // this function also can retrieve other datatyps, such as DateTime. 
                //
                // Comment by Bo
                ////DebuggerMethod(com);
                returnValue = com.ExecuteScalar();//.ToString();
            }
            catch (SqlException exSql)
            {
                if (exSql.Message.Contains("Timeout"))
                {
                    //MessageBox.Show("Sorry, the server is handling a lot of requests at the moment. \n Please try again in a few seconds.");
                }
                else
                {
                    //MessageBox.Show("SQL:" + exSql.ToString());
                }

            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }
            finally
            {
                //if (con.State == ConnectionState.Open) con.Close();
                com.Dispose();
                //con.Dispose();
            }
            return returnValue == null ? "" : returnValue;
        }
        public static object GetValue(string sql, string server)
        {
            bool isNeedClose = false;
            SqlConnection con = getConn(server, ref isNeedClose);
            object value = GetValue(sql, con);
            if (isNeedClose && con.State == ConnectionState.Open)
            {
                con.Close();
                con.Dispose();
            }
            return value;

        }

        public static object GetValue(string sql, string server, object[,] parameters)
        {
            bool isNeedClose = false;
            SqlConnection con = getConn(server, ref isNeedClose);
            object value = GetValue(sql, con, parameters);
            if (isNeedClose && con.State == ConnectionState.Open)
            {
                con.Close();
                con.Dispose();
            }
            return value;

        }

        public static void ExecuteNonQuery(string sql, string server)
        {
            bool isNeedClose = false;
            SqlConnection con = getConn(server, ref isNeedClose);
            ExecuteNonQuery(sql, con);
            if (isNeedClose && con.State == ConnectionState.Open)
            {
                con.Close();
                con.Dispose();
            }


        }
        public static void ExecuteNonQuery(string sql, SqlConnection con)
        {

            SqlCommand com = con.CreateCommand();
            com.CommandType = CommandType.Text;
            com.CommandText = sql;
            com.CommandTimeout = 600000;
            if (con.State == ConnectionState.Broken || con.State == ConnectionState.Closed) con.Open();
            try
            {
                //con.Open();
                ////DebuggerMethod(com);
                com.ExecuteNonQuery();
            }
            catch (SqlException exSql)
            {
                if (exSql.Message.Contains("Timeout"))
                {
                    // MessageBox.Show("Sorry, the server is handling a lot of requests at the moment. \n Please try again in a few seconds.");
                }
                else
                {
                    //MessageBox.Show("SQL:" + exSql.ToString());
                }

            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }
            finally
            {
                // if (con.State == ConnectionState.Open) con.Close();
                com.Dispose();
                //con.Dispose();
            }
        }

        public static void ExecuteNonQuery(SqlCommand com, SqlConnection con)
        {
            if (con.State == ConnectionState.Broken || con.State == ConnectionState.Closed) con.Open();
            com.Connection = con;
            try
            {
                ////DebuggerMethod(com);
                com.ExecuteNonQuery();
            }
            catch (SqlException exSql)
            {
                if (exSql.Message.Contains("Timeout"))
                {
                    //MessageBox.Show("Sorry, the server is handling a lot of requests at the moment. \n Please try again in a few seconds.");
                }
                else
                {
                    //MessageBox.Show("SQL:" + exSql.ToString());
                }

            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }
            finally
            {
                com.Dispose();
            }
        }

        public static void ExecuteNonQuery(SqlCommand com, string server)
        {
            bool isNeedClose = false;
            SqlConnection con = getConn(server, ref isNeedClose);
            ExecuteNonQuery(com, con);
            if (isNeedClose && con.State == ConnectionState.Open)
            {
                con.Close();
                con.Dispose();
            }
        }

        public static void ExecuteNonQuery(string sql, string server, object[,] parameters)
        {
            bool isNeedClose = false;
            SqlConnection con = getConn(server, ref isNeedClose);
            ExecuteNonQuery(sql, con, parameters);
            if (isNeedClose && con.State == ConnectionState.Open)
            {
                con.Close();
                con.Dispose();
            }
        }
        public static void ExecuteNonQuery(string sql, SqlConnection con, object[,] parameters)
        {
            SqlCommand com = con.CreateCommand();
            com.CommandType = CommandType.Text;
            com.CommandText = sql;
            com.CommandTimeout = 600000;
            if (con.State == ConnectionState.Broken || con.State == ConnectionState.Closed) con.Open();
            if (parameters != null)
                AddParams(com, parameters);
            try
            {
                //con.Open();
                ////DebuggerMethod(com);
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // if (con.State == ConnectionState.Open) con.Close();
                com.Dispose();
                //con.Dispose();
            }
        }

        public static DataTable GetTableFrom(string sql, SqlConnection con)
        {
            DataTable table = new DataTable();
            //SqlConnection con = new SqlConnection();
            //con.ConnectionString = ConfigurationManager.ConnectionStrings[source];
            SqlCommand com = con.CreateCommand();
            com.CommandType = CommandType.Text;
            com.CommandText = @sql;
            com.CommandTimeout = 6000000;

            if (con.State == ConnectionState.Broken || con.State == ConnectionState.Closed) con.Open();
            SqlDataAdapter sqldata = new SqlDataAdapter();
            try
            {
                //con.Open();
                ////DebuggerMethod(com);
                sqldata.SelectCommand = com;
                sqldata.Fill(table);
            }
            catch (SqlException exSql)
            {
                if (exSql.Message.Contains("Timeout"))
                {
                    //MessageBox.Show("Sorry, the server is handling a lot of requests at the moment. \n Please try again in a few seconds.");
                }
                else
                {
                    //MessageBox.Show("SQL:" + exSql.ToString());
                }

            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }
            finally
            {
                //if (con.State == ConnectionState.Open) con.Close();
                com.Dispose();
                //con.Dispose();
            }
            return table;
        }

        public static DataTable GetTableFrom(SqlCommand com, SqlConnection con)
        {
            DataTable table = new DataTable();
            //con.CreateCommand();
            com.Connection = con;
            com.CommandTimeout = 6000000;
            if (con.State == ConnectionState.Broken || con.State == ConnectionState.Closed) con.Open();
            SqlDataAdapter sqldata = new SqlDataAdapter();
            try
            {
                ////DebuggerMethod(com);
                sqldata.SelectCommand = com;
                sqldata.Fill(table);
            }
            catch (SqlException exSql)
            {
                if (exSql.Message.Contains("Timeout"))
                {
                    //MessageBox.Show("Sorry, the server is handling a lot of requests at the moment. \n Please try again in a few seconds.");
                }
                else
                {
                    //MessageBox.Show("SQL:" +exSql.ToString());
                }

            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }
            finally
            {
                com.Dispose();
            }
            return table;
        }

        public static DataTable GetTableFrom(SqlCommand com, string source)
        {
            bool isNeedClose = false;
            DataTable table = new DataTable();
            SqlConnection con = getConn(source, ref isNeedClose);
            table = GetTableFrom(com, con);
            return table;
        }

        public static DataTable GetTableFrom(string sql, string source)
        {
            bool isNeedClose = false;
            SqlConnection con = getConn(source, ref isNeedClose);
            DataTable table = GetTableFrom(sql, con);
            if (isNeedClose && con.State == ConnectionState.Open)
            {
                con.Close();
                con.Dispose();
            }
            return table;
        }

        public static DataTable GetTableFrom(string sql, string source, object[,] parameters)
        {
            bool isNeedClose = false;
            //using (TaskTimer timer = new TaskTimer(source, sql, parameters))
            //{
            SqlConnection con = getConn(source, ref isNeedClose);
            DataTable table = GetTableFrom(sql, con, parameters);
            if (isNeedClose && con.State == ConnectionState.Open)
            {
                con.Close();
                con.Dispose();
            }
            return table;
            //}
        }
        //DataReader Test Begin
        public static DataTable GetTableFromDtaRdr(string sql, string source, object[,] parameters)
        {
            bool isNeedClose = false;
            SqlConnection con = getConn(source, ref isNeedClose);
            DataTable table = GetTableFromDtaRdr(sql, con, parameters);
            if (isNeedClose && con.State == ConnectionState.Open)
            {
                con.Close();
                con.Dispose();
            }
            return table;
        }
        //DataReader Test End
        public static DataSet GetDataSetFromSP(string storedprocedure, string source, string[] tables, object[,] parameters)
        {
            bool isNeedClose = false;
            //using (TaskTimer timer = new TaskTimer(source, storedprocedure, parameters))
            //{
            string sql = setquery(storedprocedure, parameters);
            SqlConnection con = getConn(source, ref isNeedClose);
            DataSet dataset = getDataSet(sql, con, tables, parameters);
            if (isNeedClose && con.State == ConnectionState.Open)
            {
                con.Close();
                con.Dispose();
            }
            return dataset;
            //}
        }
        public static DataSet GetDataSetFromSP(string storedprocedure, string source, object[,] parameters)
        {
            var xarr_parameter = new List<SqlParameter>();
            for (int x = 0; x < parameters.GetLength(0); x++)
            {
                var xins_parameter = new SqlParameter();
                xins_parameter.ParameterName = parameters[x, 0].ToString();
                xins_parameter.Value = parameters[x, 1];
                xarr_parameter.Add(xins_parameter);
            }
            return GetDataSetFromSP(storedprocedure, source, xarr_parameter);
        }
        public static DataSet GetDataSetFromSP(string storedprocedure, string source, List<SqlParameter> xarr_parameter)
        {
            bool isNeedClose = false;
            DataSet dataset = new DataSet();
            SqlConnection xconnection = getConn(source, ref isNeedClose);
            SqlCommand xcommand = xconnection.CreateCommand();
            xcommand.CommandText = storedprocedure;
            xcommand.CommandType = CommandType.StoredProcedure;
            xcommand.Parameters.AddRange(xarr_parameter.ToArray());
            SqlDataAdapter xadapter = new SqlDataAdapter(xcommand);
            xadapter.Fill(dataset);

            if (isNeedClose && xconnection.State == ConnectionState.Open)
            {
                xconnection.Close();
                xconnection.Dispose();
            }

            return dataset;
        }
        public static DataTable GetTableFromSP(string storedprocedure, string source, object[,] parameters)
        {
            bool isNeedClose = false;
            string sql = setquery(storedprocedure, parameters);
            SqlConnection con = getConn(source, ref isNeedClose);
            DataTable table = GetTableFrom(sql, con, parameters);
            if (isNeedClose && con.State == ConnectionState.Open)
            {
                con.Close();
                con.Dispose();
            }
            return table;
        }
        public static DataTable GetTableFromSP(string storedprocedure, string source, List<SqlParameter> xarr_parameter)
        {
            DataTable xret = new DataTable();
            bool isNeedClose = false;

            SqlConnection xconnection = getConn(source, ref isNeedClose);
            SqlCommand xcommand = xconnection.CreateCommand();
            xcommand.CommandText = storedprocedure;
            xcommand.CommandType = CommandType.StoredProcedure;
            xcommand.Parameters.AddRange(xarr_parameter.ToArray());

            try
            {
                //DebuggerMethod(xcommand);
                SqlDataAdapter xadapter = new SqlDataAdapter(xcommand);
                xadapter.Fill(xret);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (isNeedClose && xconnection.State == ConnectionState.Open)
            {
                xconnection.Close();
                xconnection.Dispose();
            }

            return xret;
        }
        private static string setquery(string sp, object[,] parameters)
        {
            string query = "exec dbo." + sp + " ";
            if (parameters != null)
                for (int i = 0; i < parameters.GetLength(0); i++)
                    query = query + parameters[i, 0].ToString() + ",";
            query = query.TrimEnd(',');
            return query + " ";
        }

        public static DataTable GetTableFrom(string sql, SqlConnection con, object[,] parameters)
        {
            DataTable table = new DataTable();
            //SqlConnection con = new SqlConnection();
            //con.ConnectionString = ConfigurationManager.ConnectionStrings[source];
            SqlCommand com = con.CreateCommand();
            com.CommandType = CommandType.Text;
            com.CommandText = sql;
            com.CommandTimeout = 6000000;
            if (con.State == ConnectionState.Broken || con.State == ConnectionState.Closed) con.Open();
            SqlDataAdapter sqldata = new SqlDataAdapter();
            if (parameters != null)
                AddParams(com, parameters);
            try
            {
                //DebuggerMethod(com);
                //con.Open();
                sqldata.SelectCommand = com;


                //DateTime t1 = DateTime.Now; 

                sqldata.Fill(table);

                //DateTime t2 = DateTime.Now;

                //MessageBox.Show(Convert.ToString(t2 - t1));
            }
            catch (SqlException exSql)
            {
                if (exSql.Message.Contains("Timeout"))
                {
                    //MessageBox.Show("Sorry, the server is handling a lot of requests at the moment. \n Please try again in a few seconds.");
                }
                else
                {
                    //MessageBox.Show("SQL:" + exSql.ToString());
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                throw ex;
            }
            finally
            {
                //if (con.State == ConnectionState.Open) con.Close();
                com.Dispose();
                //con.Dispose();
            }
            return table;
        }

        public static void AddParams(SqlCommand com, object[,] parameters)
        {
            for (int i = 0; i < parameters.GetLength(0); i++)
                com.Parameters.AddWithValue(parameters[i, 0].ToString(), parameters[i, 1]);
        }
        //Reader Test Begin
        public static DataTable GetTableFromDtaRdr(string sql, SqlConnection con, object[,] parameters)
        {
            DataTable table = new DataTable();
            SqlCommand com = con.CreateCommand();
            com.CommandType = CommandType.Text;
            com.CommandText = sql;
            com.CommandTimeout = 6000000;
            if (con.State == ConnectionState.Broken || con.State == ConnectionState.Closed) con.Open();
            //SqlDataAdapter sqldata = new SqlDataAdapter();            
            if (parameters != null)
                for (int i = 0; i < parameters.GetLength(0); i++)
                    com.Parameters.AddWithValue(parameters[i, 0].ToString(), parameters[i, 1]);

            SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
            try
            {
                //DebuggerMethod(com);
                //    sqldata.SelectCommand = com;
                //    sqldata.Fill(table);
                table.Load(dr);
            }
            catch (SqlException exSql)
            {
                if (exSql.Message.Contains("Timeout"))
                {
                    //MessageBox.Show("Sorry, the server is handling a lot of requests at the moment. \n Please try again in a few seconds.");
                }
                else
                {
                    //MessageBox.Show("SQL:" + exSql.ToString());
                }

            }
            catch (Exception ex)
            {
                throw ex;//Model.Log.Error(ex);
            }
            finally
            {
                com.Dispose();
            }
            return table;
        }
        //Reader Test End
        public static DataSet getDataSet(SqlCommand com, SqlConnection con)
        {
            DataSet table = new DataSet();
            com.Connection = con;
            SqlDataAdapter sqldata = new SqlDataAdapter();
            if (con.State == ConnectionState.Broken || con.State == ConnectionState.Closed) con.Open();
            try
            {
                //DebuggerMethod(com);
                sqldata.SelectCommand = com;
                sqldata.Fill(table);
            }
            catch (SqlException exSql)
            {
                if (exSql.Message.Contains("Timeout"))
                {
                    //MessageBox.Show("Sorry, the server is handling a lot of requests at the moment. \n Please try again in a few seconds.");
                }
                else
                {
                    //MessageBox.Show("SQL:" + exSql.ToString());
                }

            }
            catch
            {
                // MessageBox.Show(ex.ToString());
            }
            finally
            {
                com.Dispose();
            }
            return table;
        }

        public static DataSet getDataSet(SqlCommand com, string source)
        {
            bool isNeedClose = false;
            DataSet table = new DataSet();
            SqlConnection con = getConn(source, ref isNeedClose);
            table = getDataSet(com, con);
            return table;
        }

        public static DataSet getDataSet(string sql, string source)
        {
            bool isNeedClose = false;
            DataSet table = new DataSet();
            SqlCommand com = new SqlCommand(sql);
            com.CommandType = CommandType.Text;
            SqlConnection con = getConn(source, ref isNeedClose);
            table = getDataSet(com, con);
            return table;
        }

        public static DataSet getDataSet(string sql, string server, string[] tableNames, object[,] parameters)
        {
            bool isNeedClose = false;
            //using (TaskTimer timer = new TaskTimer(server, sql, parameters))
            //{
            SqlConnection con = getConn(server, ref isNeedClose);
            DataSet set = getDataSet(sql, con, tableNames, parameters);
            if (isNeedClose && con.State == ConnectionState.Open)
            {
                con.Close();
                con.Dispose();
            }
            return set;
            //}
        }
        public static DataSet getDataSet(string sql, SqlConnection con, string[] tableNames, object[,] parameters)
        {
            //DataTable table = new DataTable();

            DataSet set = new DataSet();
            //SqlConnection con = new SqlConnection();
            //con.ConnectionString = ConfigurationManager.ConnectionStrings[source];
            SqlCommand com = con.CreateCommand();
            com.CommandType = CommandType.Text;
            com.CommandText = sql;
            SqlDataAdapter sqldata = new SqlDataAdapter();
            if (con.State == ConnectionState.Broken || con.State == ConnectionState.Closed) con.Open();
            if (parameters != null)
                for (int i = 0; i < parameters.GetLength(0); i++)
                    com.Parameters.AddWithValue(parameters[i, 0].ToString(), parameters[i, 1]);
            try
            {
                //DebuggerMethod(com);
                //con.Open();
                sqldata.SelectCommand = com;
                sqldata.Fill(set);
            }
            catch (SqlException exSql)
            {
                if (exSql.Message.Contains("Timeout"))
                {
                    //MessageBox.Show("Sorry, the server is handling a lot of requests at the moment. \n Please try again in a few seconds.");
                }
                else
                {
                    //MessageBox.Show("SQL:" + exSql.ToString());
                }

            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }
            finally
            {
                //if (con.State == ConnectionState.Open) con.Close();
                com.Dispose();
                //con.Dispose();
            }
            //return table;
            if (tableNames != null)
                for (int i = 0; i < set.Tables.Count && i < tableNames.Length; i++)
                    set.Tables[i].TableName = tableNames[i];

            return set;
        }

        public static DataTable FilterTable(DataTable dt, string filterString)
        {
            DataRow[] filteredRows = dt.Select(filterString);
            DataTable filteredDt = dt.Clone();

            DataRow dr;
            foreach (DataRow oldDr in filteredRows)
            {
                dr = filteredDt.NewRow();
                for (int i = 0; i < dt.Columns.Count; i++)
                    dr[dt.Columns[i].ColumnName] = oldDr[dt.Columns[i].ColumnName];
                filteredDt.Rows.Add(dr);
            }
            return filteredDt;
        }

        public static void addTablePara(DataTable table, SqlConnection con, string temName, string selection)
        {
            if (temName.StartsWith("#"))
            {
                string SQL = SqlTableCreator.GetCreateFromDataTableSQL(temName, table);
                ExecuteNonQuery(SQL, con);
                //ExecuteNonQuery("delete from TEST001", con);

                if (selection != null)
                    BulkLoad(FilterTable(table,
                        selection), temName, con);
                else
                    BulkLoad(table, temName, con);
            }
            else
            {
                // MessageBox.Show("Only Temp table allowed");
            }
        }

        public static SqlConnection getConn(string server)
        {

            string CnnString;
            switch (server.ToUpper())
            {
                case "NetCRMAU": CnnString = DataSource.ConnNetCRMAU.ConnectionString; break;

                case "LIVEXACTAUS": CnnString = DataSource.ConnLivExactAUS.ConnectionString; break;

                case "UniChoice": CnnString = DataSource.ConnWeb.ConnectionString; break;

                default:
                    CnnString = ConfigurationManager.ConnectionStrings[server].ConnectionString;
                    break;

            }
            SqlConnection con = getConnFromString(CnnString);

            return con;
        }

        public static SqlConnection getConn(string server, ref bool isNeedClose)
        {
            SqlConnection con;
            switch (server.ToUpper())
            {
                case "NetCRMAU-": con = DataSource.ConnNetCRMAU; break;

                case "LIVEXACTAUS-": con = DataSource.ConnLivExactAUS; break;

                case "UniChoice-": con = DataSource.ConnWeb; break;

                case "NETUniChoice-": con = DataSource.ConnNetUniChoice; break;

                default:
                    con = new SqlConnection();
                    if (ConfigurationManager.AppSettings[server] != null && ConfigurationManager.AppSettings["version"] != null)
                        con.ConnectionString = "Application Name=liv_new-" + ConfigurationManager.AppSettings["version"] + ";" + ConfigurationManager.ConnectionStrings[server].ConnectionString;
                    else if (server.ToLower() == "netcrmau")
                        con.ConnectionString = "Server=NetCRMAU;Database=netcrm;uid=dev";
                    else if (server.ToLower() == "livexactaus")
                        con.ConnectionString = "Server=LivExactAUS;Database=135;uid=dev";
                    else
                    {
                        con.ConnectionString = "Server=123.11.8.218;Database=unichoice;uid=dev;";
                    }
                    con.Open();
                    isNeedClose = true;
                    break;

            }
            return con;
        }

        public static SqlConnection getConnFromString(string strCon)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = strCon;
            con.Open();

            return con;
        }

        public static void BulkLoad(DataTable tbl, string TableName, SqlConnection connection)
        {
            //string conn = ConfigurationManager.ConnectionStrings["NetCRMAU"];

            System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(connection);

            bulkCopy.DestinationTableName = TableName;

            try
            {
                //DebuggerMethod(new SqlCommand() { CommandText = "BULKLOAD" });
                bulkCopy.WriteToServer(tbl);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                bulkCopy.Close();
            }
        }

        public static void initDataBase(int type, string User)
        {

            DataSource.ConnNetUniChoice = DataSource.getConnFromString
                            ("Application Name=NetCRM-" + User + ";Server=NetCRM;Database=UniChoice;uid=dev;");
            switch (type)
            {
                case (int)Database.DBaseType.dbExactPH:

                    DataSource.ConnNetCRMAU = DataSource.getConnFromString
                            ("Application Name=NetCRM-" + User + ";Server=ExactPH;Database=NetCRM;uid=dev;");
                    DataSource.ConnLivExactAUS = DataSource.getConnFromString
                            ("Application Name=NetCRM-" + User + ";Server=LivExact2;Database=135;uid=dev;");
                    DataSource.ConnWeb = DataSource.getConnFromString
                        //("Application Name=NetCRM-" + User + ";Server=ExactPH;Database=unichoice;uid=dev;");
                    ("Application Name=NetCRM-" + User + ";Server=livingwebrent;Database=unichoice;uid=dev;");


                    break;
                case (int)Database.DBaseType.dbLivMacola:
                    DataSource.ConnNetCRMAU = DataSource.getConnFromString
                            ("Application Name=NetCRM-" + User + ";Server=LivMacola;Database=NetCRM;uid=dev;");
                    DataSource.ConnLivExactAUS = DataSource.getConnFromString
                            ("Application Name=NetCRM-" + User + ";Server=Livexact3\\LIVEHK;Database=135;uid=dev;");
                    DataSource.ConnWeb = DataSource.getConnFromString
                        //("Application Name=NetCRM-" + User + ";Server=LivMacola;Database=unichoice;uid=dev;");
                    ("Application Name=NetCRM-" + User + ";Server=livingwebrent;Database=unichoice;uid=dev;");

                    break;

                default:
                    DataSource.ConnNetCRMAU = DataSource.getConnFromString
                             ("Application Name=NetCRM-" + User + ";Server=NetCRMAU;Database=NetCRM;uid=dev;");
                    DataSource.ConnLivExactAUS = DataSource.getConnFromString
                            ("Application Name=NetCRM-" + User + ";Server=LivExactAus;Database=135;uid=dev;");
                    DataSource.ConnWeb = DataSource.getConnFromString
                            ("Application Name=NetCRM-" + User + ";Server=livingwebrent;Database=unichoice;uid=dev;");

                    break;
            }
        }

    }
}
