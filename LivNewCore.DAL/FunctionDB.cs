﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Drawing;
using System.Data;

namespace LivNewCore.DAL
{
    public class FunctionDB
    {
        public static T InitFromDataRow<T>(DataRow xrow)
        {
            T xret = Activator.CreateInstance<T>();

            FillFromDataRow(xret, xrow);

            return xret;
        }

        public static void FillFromDataRow(object xinstance, DataRow xrow)
        {
            if (xinstance == null
                || xrow == null)
            {
                return;
            }

            Type xtype = xinstance.GetType();

            foreach (PropertyInfo xinfo in xtype.GetProperties())
            {
                if (xrow.Table.Columns.Contains(xinfo.Name))
                {
                    try
                    {
                        if (xinfo.PropertyType.Name == "String")
                        {
                            xinfo.SetValue(xinstance, (xrow[xinfo.Name] ?? "").ToString(), null);
                        }
                        else if (xinfo.PropertyType.Name == "Boolean")
                        {
                            int xtmp = 0;
                            if ((xrow[xinfo.Name] ?? "False").ToString() == "True")
                            {
                                xtmp = 1;
                            }
                            else
                            {
                                int.TryParse((xrow[xinfo.Name] ?? "0").ToString(), out xtmp);
                            }
                            xinfo.SetValue(xinstance, (xtmp != 0), null);
                        }
                        else if (xinfo.PropertyType.Name == "Image")
                        {
                            if (!(xrow[xinfo.Name] is DBNull))
                            {
                                Image img = (Bitmap)(new ImageConverter()).ConvertFrom(xrow[xinfo.Name]);
                                xinfo.SetValue(xinstance, img, null);
                            }
                        }
                        else if (xinfo.PropertyType.Name == "DateTime")
                        {
                            DateTime dt = DateTime.Parse(xrow[xinfo.Name].ToString());
                            xinfo.SetValue(xinstance, dt, null);
                        }
                        else if (!(xrow[xinfo.Name] is DBNull))
                        {
                            xinfo.SetValue(xinstance, xrow[xinfo.Name], null);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
        public static void SetReflectionValue(object xinstance, PropertyInfo xinfo, object value)
        {
            if (xinfo != null)
            { //xins_info.GetValue(xinstance, null).Equals(Activator.CreateInstance(xins_info.PropertyType)))
                if (xinfo.PropertyType.Name == "String")
                {
                    xinfo.SetValue(xinstance, (value ?? "").ToString(), null);
                }
                else if (xinfo.PropertyType.Name == "Boolean")
                {
                    int xtmp = 0;
                    if ((value ?? "False").ToString() == "True")
                    {
                        xtmp = 1;
                    }
                    else
                    {
                        int.TryParse((value ?? "0").ToString(), out xtmp);
                    }
                    xinfo.SetValue(xinstance, (xtmp != 0), null);
                }
                else if (xinfo.PropertyType.Name == "Image")
                {
                    if (!(value is DBNull))
                    {
                        Bitmap img = (Bitmap)(new ImageConverter()).ConvertFrom(value);
                        xinfo.SetValue(xinstance, img, null);
                    }
                }
                else if (!(value is DBNull))
                {
                    xinfo.SetValue(xinstance, value, null);
                }
            }
        }

        public static string ToString(DataRow row, string colName)
        {
            if (row[colName] == DBNull.Value)
                return string.Empty;

            return row[colName].ToString();
        }

        public static T To<T>(DataRow row, string colName)
        {
            if (row[colName] == DBNull.Value)
                return default(T);

            object rowValue = row[colName];
            if (rowValue.GetType().IsAssignableFrom(typeof(T)))
                return (T)rowValue;

            return default(T);
        }

        public static void Insert(object xinstance, string xtablename, string xdatabase)
        {
            FunctionDB.Insert(xinstance, null, xtablename, xdatabase);
        }
        public static void Insert(object xinstance, string[] xarr_exclude, string xtablename, string xdatabase)
        {
            if (xinstance == null)
            {
                return;
            }

            Type xtype = xinstance.GetType();
            List<PropertyInfo> xlist_info = new List<PropertyInfo>();
            foreach (PropertyInfo xins_info in xtype.GetProperties())
            {
                switch (xins_info.PropertyType.Name.ToLower())
                {
                    case "string":
                        break;
                    case "char":
                    case "float":
                    case "double":
                    case "int32":
                    case "int64":
                    case "uint32":
                    case "uint64":
                    case "decimal":
                    case "bool":
                    case "boolean":
                    case "datetime":
                    case "timespan":
                        if (xins_info.GetValue(xinstance, null).Equals(Activator.CreateInstance(xins_info.PropertyType)))
                        {
                            continue;
                        }
                        break;
                    default:
                        continue;
                }

                if (xins_info.GetValue(xinstance, null) is DBNull)
                {
                    continue;
                }

                if (xins_info.GetValue(xinstance, null) == null)
                {
                    continue;
                }

                if (xarr_exclude != null
                    && xarr_exclude.Contains(xins_info.Name))
                {
                    continue;
                }

                xlist_info.Add(xins_info);
            }

            if (xlist_info.Count == 0)
            {
                return;
            }

            object[,] xarr_param = new object[xlist_info.Count, 2];
            for (int xctr = 0; xctr < xlist_info.Count; xctr++)
            {
                xarr_param[xctr, 0] = "@" + xlist_info[xctr].Name;
                xarr_param[xctr, 1] = xlist_info[xctr].GetValue(xinstance, null);
            }

            DataSource.ExecuteNonQuery(string.Format(@"
                    INSERT INTO {0}
                    (
                        {1}
                    )
                    VALUES
                    (
                        {2}
                    )
                ",
                 xtablename,
                 string.Join(",", xlist_info.Select(xtmp => xtmp.Name).ToArray()),
                 string.Join(",", xlist_info.Select(xtmp => "@" + xtmp.Name).ToArray())
                ),
                 xdatabase,
                 xarr_param
             );
        }

        public static void Update(object xinstance, string xtablename, string xdatabase, string xfilter, params object[] xarr_filter)
        {
            FunctionDB.Update(xinstance, null, xtablename, xdatabase, xfilter, xarr_filter);
        }
        public static void Update(object xinstance, string[] xarr_exclude, string xtablename, string xdatabase, string xfilter, params object[] xarr_filter)
        {
            if (xinstance == null)
            {
                return;
            }

            Type xtype = xinstance.GetType();
            List<PropertyInfo> xlist_info = new List<PropertyInfo>();
            foreach (PropertyInfo xins_info in xtype.GetProperties())
            {
                switch (xins_info.PropertyType.Name.ToLower())
                {
                    case "string":
                        break;
                    case "char":
                    case "float":
                    case "double":
                    case "int32":
                    case "int64":
                    case "uint32":
                    case "uint64":
                    case "decimal":
                    case "bool":
                    case "boolean":
                    case "datetime":
                    case "timespan":
                        if (xins_info.GetValue(xinstance, null).Equals(Activator.CreateInstance(xins_info.PropertyType)))
                        {
                            continue;
                        }
                        break;
                    default:
                        continue;
                }

                if (xins_info.GetValue(xinstance, null) is DBNull)
                {
                    continue;
                }

                if (xins_info.GetValue(xinstance, null) == null)
                {
                    continue;
                }

                if (xarr_exclude != null
                    && xarr_exclude.Contains(xins_info.Name))
                {
                    continue;
                }

                xlist_info.Add(xins_info);
            }

            if (xlist_info.Count == 0)
            {
                return;
            }

            if (xarr_filter == null)
            {
                xarr_filter = new object[0];
            }

            object[,] xarr_param = new object[xlist_info.Count + xarr_filter.Length, 2];
            for (int xctr = 0; xctr < xlist_info.Count; xctr++)
            {
                xarr_param[xctr, 0] = xlist_info[xctr].Name;
                xarr_param[xctr, 1] = xlist_info[xctr].GetValue(xinstance, null);
            }

            for (int xctr = 0; xctr < xarr_filter.Length; xctr++)
            {
                xarr_param[xctr + xlist_info.Count, 0] = "@" + xctr.ToString();
                xarr_param[xctr + xlist_info.Count, 1] = xarr_filter[xctr];
            }

            if ((xfilter ?? "").ToString() == "")
            {
                xfilter = "1=1";
            }

            DataSource.ExecuteNonQuery(string.Format(@"
                    UPDATE {0}
                    SET {1}
                    WHERE {2}
                ",
                 xtablename,
                 string.Join(",",
                    xlist_info.Select(xtmp => xtmp.Name + "=" + "@" + xtmp.Name).ToArray()
                 ),
                 xfilter
                ),
                 xdatabase,
                 xarr_param
             );
        }
        public static void Update2(object xtablename, string xdatabase, string xsetfields, string xfilter, params object[] xarr_filter)
        {
            object[,] xarr_param = new object[xarr_filter.Length, 2];

            for (int xctr = 0; xctr < xarr_filter.Length; xctr++)
            {
                xarr_param[xctr, 0] = "@" + xctr.ToString();
                xarr_param[xctr, 1] = xarr_filter[xctr];
            }

            if ((xfilter ?? "").ToString() == "")
            {
                xfilter = "1=1";
            }

            DataSource.ExecuteNonQuery(string.Format(@"
                    UPDATE {0}
                    SET {1}
                    WHERE {2}
                ",
                 xtablename,
                 xsetfields,
                 xfilter
                ),
                 xdatabase,
                 xarr_param
             );
        }
        public static void Delete(object xtablename, string xdatabase, string xfilter, params object[] xarr_filter)
        {
            object[,] xarr_param = new object[xarr_filter.Length, 2];

            for (int xctr = 0; xctr < xarr_filter.Length; xctr++)
            {
                xarr_param[xctr, 0] = "@" + xctr.ToString();
                xarr_param[xctr, 1] = xarr_filter[xctr];
            }

            if ((xfilter ?? "").ToString() == "")
            {
                xfilter = "1=1";
            }

            DataSource.ExecuteNonQuery(string.Format(@"
                    DELETE
                    FROM {0}
                    WHERE {1}
                ",
                 xtablename,
                 xfilter
                ),
                 xdatabase,
                 xarr_param
             );
        }

        public static List<string> GetEditedFields<T>(T original, T edited, string[] to_compare)
        {
            List<string> xresult = new List<string>();
            Type xtype = typeof(T);
            foreach (PropertyInfo xins_info in xtype.GetProperties())
            {
                if (to_compare.ToList().ConvertAll(s => s.ToLowerInvariant()).Contains(xins_info.Name.ToLower()))
                {
                    object original_val = xins_info.GetValue(original, null);
                    object edited_val = xins_info.GetValue(edited, null);

                    if (xins_info.PropertyType.Name.ToLower().Equals("string"))
                    {
                        if (!(original_val.ToString().Trim()
                            .Equals(edited_val == null ? string.Empty : edited_val.ToString().Trim())))
                        {
                            xresult.Add(xins_info.Name);
                        }
                    }
                    else
                    {
                        if (!(original_val.Equals(edited_val)))
                        {
                            xresult.Add(xins_info.Name);
                        }
                    }
                }
            }

            return xresult;
        }
    }
}
