﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;

namespace LivNewCore.DAL
{
    public abstract class Database
    {
        private SqlConnection mCon;
        private static string sqlErrorException = "Subquery returned more than 1 value. This is not permitted when the subquery follows =, !=, <, <= , >, >= or when the subquery is used as an expression.";
        private static string formname = "Database - Database.cs";
        private static string mName;
        private static ArrayList ConnectionPool;
        private string type = "NetCRM";
        //private LivConnection currentConnection = null;
        public static string User = "";


        public enum DBaseType
        {
            dbNetCRMAU,
            dbLivMacola,
            dbExactPH,
            dbNone
        }
        public void Dispose()
        {

        }

        public static DBaseType mDBaseUse;

        //        public Database(string pConnectionString)
        //        {

        //            if (pConnectionString.Contains("%s")) this.type = "NetCRM";
        //            else if (pConnectionString.Contains("%e")) this.type = "Exact";
        //            else if (pConnectionString.Contains("%e")) this.type = "Web";
        //            else if (pConnectionString.Contains("unichoice")) this.type = "UniChoice";
        //            else if (pConnectionString.Contains("Livingdata")) this.type = "Livingdata";
        //            else if (pConnectionString.Contains("NetCRM")) this.type = "NetCRM2";

        //            string ConnStr = pConnectionString.Trim();
        //                if(DBaseUse == DBaseType.dbLivMacola)
        //            {
        //                // Hong Kong
        //                ConnStr = Regex.Replace(ConnStr, "%s", "LivMacola", RegexOptions.IgnoreCase);
        //                ConnStr = Regex.Replace(ConnStr, "%e", "Livexact3\\LIVEHK", RegexOptions.IgnoreCase);
        //                ConnStr = Regex.Replace(ConnStr, "%w", "LivMacola", RegexOptions.IgnoreCase); // for WEB database
        //            }
        //            else if(DBaseUse == DBaseType.dbNetCRMAU)
        //            {
        //                // Australia
        //                ConnStr = Regex.Replace(ConnStr, "%s", "NetCRMAU", RegexOptions.IgnoreCase);
        //                ConnStr = Regex.Replace(ConnStr, "%e", "LivExactAus", RegexOptions.IgnoreCase);
        //                ConnStr = Regex.Replace(ConnStr, "%w", "LivingWebRent", RegexOptions.IgnoreCase); // for WEB database
        //            }
        //            else if(DBaseUse == DBaseType.dbExactPH)
        //            {
        //                //Phil
        //                ConnStr = Regex.Replace(ConnStr, "%s", "ExactPH", RegexOptions.IgnoreCase);
        //                ConnStr = Regex.Replace(ConnStr, "%e", "LivExact2", RegexOptions.IgnoreCase);
        //                ConnStr = Regex.Replace(ConnStr, "%w", "ExactPH", RegexOptions.IgnoreCase); // for WEB database
        //            }
        //            try
        //            {
        //                ConnStr = "Application Name=NetCRM-" + User + "-;" + ConnStr;

        //                if (ConnectionPool == null)
        //                {
        //                    ConnectionPool = new ArrayList();
        //                }
        //                LivConnection tempCon = new LivConnection(true, this.type);

        //                int searchResult = ConnectionPool.BinarySearch(tempCon, new ConnectionCompare());

        //                if (searchResult >= 0 && searchResult < ConnectionPool.Count )
        //                {
        //                    //mCon = ((LivConnection)ConnectionPool[searchResult]).con;
        //                    this.currentConnection = (LivConnection)ConnectionPool[searchResult];
        //                    mCon = currentConnection.con;


        //                    if (mCon.State == ConnectionState.Open )
        //                    {
        //                        mCon.Close();
        //                        ConnectionPool.Remove(this.currentConnection);
        //                    }

        //                    if (mCon.State == ConnectionState.Closed )
        //                        ConnectionPool.Remove(this.currentConnection);
        //                }   

        //                {
        //                    mCon = new SqlConnection();
        //                    mCon.ConnectionString = ConnStr;
        //                    mCon.Open();
        //                    tempCon.assignID();
        //                    tempCon.con = mCon;
        //                    //if (this.type.ToUpper() == "NETCRM" )
        //                    //{ if (LivConnection.iCountNetCRM++ < 5) tempCon.isFirst = true; }
        //                    //else if (this.type.ToUpper() == "EXACT")
        //                    //{ if (LivConnection.iCountExact++ < 5) tempCon.isFirst = true; }
        //                    //else if (this.type.ToUpper() == "WEB")
        //                    //{ if (LivConnection.iCountWeb++ < 5) tempCon.isFirst = true; }
        //                    //else if (this.type.ToUpper() == "LIVINGDATA")
        //                    //{ if (LivConnection.iCountLivingData++ < 5) tempCon.isFirst = true; }
        //                    //else if (this.type.ToUpper() == "NETCRM2")
        //                    //{ if (LivConnection.iCountNetCRM2++ < 5) tempCon.isFirst = true; }
        //                    //else if (this.type.ToUpper() == "UniChoice")
        //                    //{ if (LivConnection.iCountNetCRM2++ < 5) tempCon.isFirst = true; }

        //                    tempCon.isFirst = false;

        //                    ConnectionPool.Add(tempCon);
        //                    this.currentConnection = tempCon;



        //                }

        //                //if (mCon.State != ConnectionState.Open && mCon.State != ConnectionState.Connecting
        //                //   && mCon.State != ConnectionState.Executing && mCon.State != ConnectionState.Fetching)
        //                //{
        //                //    mCon.ConnectionString = ConnStr;
        //                //    mCon.Open();
        //                //}
        //                currentConnection.isActive = false;

        //            }
        //            catch(Exception ex)
        //            {
        //                MessageBox.Show(ex.Message.ToString(),"NetCRM");
        //                Errorlog(ex.Message.ToString(),formname,"public Database(string pConnectionString) - " + ex.TargetSite.ToString());
        //            }
        //        }

        //        ~Database()
        //        {
        //            Dispose(false);
        //        }

        //        public void Dispose()
        //        {			
        //            Dispose(true);
        //            GC.SuppressFinalize(this);
        //        }

        //        protected virtual void Dispose(bool disposing)
        //        {
        //            if (this.currentConnection != null)
        //            {


        //                this.currentConnection.isActive = true;

        //                    try
        //                    {
        //                        if (this.currentConnection.con.State == ConnectionState.Open)
        //                            this.currentConnection.con.Close();

        //                        if (this.currentConnection.con.State == ConnectionState.Closed) 
        //                            ConnectionPool.Remove(this.currentConnection);
        //                    }
        //                    catch { }

        //            }


        //            if (disposing)
        //            {



        //                try
        //                {
        //                    if (mCon.State != ConnectionState.Open) mCon.Close();		
        //                }
        //                catch(Exception ex)
        //                {
        //                    MessageBox.Show(ex.Message.ToString(),"NetCRM");
        //                    Errorlog(ex.Message.ToString(),formname,"protected virtual void Dispose(bool disposing) - " + ex.TargetSite.ToString());
        //                }
        //                finally
        //                {
        //                    mCon.Dispose();	
        //                }
        //            }			
        //        }

        //        #region MultipleReplace
        //        /// <summary>
        //        /// Replaces all occurrences of a specified System.String in this instance, with
        //        /// another specified System.String.
        //        /// </summary>
        //        /// <param name="Variable">
        //        /// A System.String to be evaluated.
        //        /// </param>
        //        /// <param name="OldValue">
        //        /// A collection of System.String to be replaced.
        //        /// </param>
        //        /// <param name="NewValue">
        //        /// A System.String to replace all occurrences of OldValue.
        //        /// </param>
        //        /// <returns>
        //        /// A System.String equivalent to this instance but with all instances of OldValue
        //        /// replaced with NewValue.
        //        /// </returns>
        //        private static string MultipleReplace(string Variable, string[] OldValue, string NewValue)
        //        {
        //            string Result = string.Empty;
        //            if (PrepareProperText(Variable).Trim() != string.Empty && OldValue.Length > 0)
        //            {
        //                Result = Variable;
        //                for (int Iteration = 0; Iteration < OldValue.Length; Iteration++)
        //                    Result = Result.Replace(OldValue[Iteration], NewValue);
        //            }
        //            return Result;
        //        }

        //        /// <summary>
        //        /// Replaces all occurrences of a specified System.String in this instance, with
        //        /// another specified System.String.
        //        /// </summary>
        //        /// <param name="Variable">
        //        /// A System.String to be evaluated.
        //        /// </param>
        //        /// <param name="OldValue">
        //        /// A collection of System.String to be replaced.
        //        /// </param>
        //        /// <param name="NewValue">
        //        /// A collection of System.String to replace all occurrences of OldValue.
        //        /// </param>
        //        /// <returns>
        //        /// A System.String equivalent to this instance but with all instances of OldValue
        //        /// replaced with NewValue.
        //        /// </returns>
        //        private static string MultipleReplace(string Variable, string[] OldValue, params string[] NewValue)
        //        {
        //            string Result = string.Empty;
        //            if (PrepareProperText(Variable).Trim() != string.Empty && OldValue.Length > 0 && NewValue.Length > 0 && OldValue.Length == NewValue.Length)
        //            {
        //                Result = Variable;
        //                for (int Iteration = 0; Iteration < OldValue.Length; Iteration++)
        //                    Result = Result.Replace(OldValue[Iteration], NewValue[Iteration]);
        //            }
        //            return Result;
        //        }
        //        #endregion

        //        #region PrepareProperText
        //        /// <summary>
        //        /// Avoiding throwing exception when retrieving a string value from an object variable.
        //        /// </summary>
        //        /// <param name="Value">
        //        /// An object type System.Object which will evaluate.
        //        /// </param>
        //        /// <returns>
        //        /// Returns the proper string value of an object.
        //        /// </returns>
        //        private static string PrepareProperText(object Value)
        //        {
        //            try
        //            {
        //                return ((Value == null || Value == DBNull.Value) ? string.Empty : Value.ToString());
        //            }
        //            catch
        //            {
        //                return string.Empty;
        //            }
        //        }
        //        #endregion

        //        public static DBaseType DBaseUse
        //        {
        //            get {return mDBaseUse;}
        //            set {mDBaseUse = value;}
        //        }

        //        public static string LoginName
        //        {
        //            get {return mName;}
        //            set {mName = value;}
        //        }

        //        public static void Errorlog(string errormsg, string formname, string functionname)
        //        {
        //        //    NETCRM nc = new NETCRM();
        //        //    SqlCommand com = new SqlCommand();
        //        //    errormsg = errormsg.Replace("'","''");

        //        //    string SQL = "insert into ErrorLog " +
        //        //        "select '" + LoginName.Trim() + "', getdate(), '" +formname+"', '" +functionname+ "', '" +errormsg+ "', (select role from userrole where usr = '" + mName.Trim() + "')";
        //        //    com.CommandType = CommandType.Text;
        //        //    com.Connection = nc.GetSqlConnection();
        //        //    com.CommandText = SQL;
        //        //    try
        //        //    {
        //        //        com.ExecuteNonQuery();
        //        //    }
        //        //    catch(Exception e)
        //        //    {
        //        //        MessageBox.Show(e.Message.ToString(),"NetCRM");
        //        //    }
        //        //    finally
        //        //    {
        //        //        com.Dispose();
        //        //        nc.Dispose();
        //        //    }
        //        }

        //        // Gilbert Abellar --// July 18, 2008 --// Update Database from all servers (NetCRMAU, ExactPH and LivMacola servers).
        //        public static void UpdateFromAllServers(string SQL)
        //        {
        //            DBaseType CurrType = DBaseUse;
        //            DBaseType[] AllServers = { DBaseType.dbNetCRMAU /*, DBaseType.dbExactPH , DBaseType.dbLivMacola */ };
        //            foreach(DBaseType DBType in AllServers)
        //            {
        //                DBaseUse = DBType;
        //                NETCRM nc = new NETCRM();
        //                try 
        //                {
        //                    nc.Execute(SQL);
        //                }
        //                catch (SqlException eSql)
        //                {
        //                    DBaseUse = CurrType;
        //                    MessageBox.Show((eSql.Message + " at Line " + eSql.LineNumber.ToString() + " character " + eSql.Number.ToString()), "NetCRM", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //                    Errorlog(eSql.Message.ToString(),formname,"public static void UpdateFromAllServers(string SQL) - " + eSql.TargetSite.ToString());
        //                }
        //                finally
        //                {
        //                    nc.Dispose();
        //                }
        //            }
        //            DBaseUse = CurrType;
        //        }

        //        public static string PrepareProperQuery(string query)
        //        {
        //            string tpl_query = string.Empty;
        //            tpl_query = query.Trim();

        //            if(DBaseUse == DBaseType.dbLivMacola)
        //            {

        //                tpl_query = Regex.Replace(tpl_query, "NetCRMAU", "LivMacola", RegexOptions.IgnoreCase);
        //                tpl_query = Regex.Replace(tpl_query, "EXACTPH", "LivMacola", RegexOptions.IgnoreCase);
        //                tpl_query = Regex.Replace(tpl_query, @"LIVEXACT2", "LIVEXACT3\\LIVEHK", RegexOptions.IgnoreCase);
        //                tpl_query = Regex.Replace(tpl_query, @"LIVEXACTAUS", "LIVEXACT3\\LIVEHK", RegexOptions.IgnoreCase);
        //            }
        //            else if(DBaseUse == DBaseType.dbExactPH)
        //            {
        //                tpl_query = Regex.Replace(tpl_query, "NetCRMAU", "ExactPH", RegexOptions.IgnoreCase);
        //                tpl_query = Regex.Replace(tpl_query, "LIVMACOLA", "ExactPH", RegexOptions.IgnoreCase);
        //                tpl_query = Regex.Replace(tpl_query, @"LIVEXACT3\\LIVEHK", "LivExact2", RegexOptions.IgnoreCase);
        //                tpl_query = Regex.Replace(tpl_query, "LIVEXACTAUS", "LivExact2", RegexOptions.IgnoreCase);
        //            }
        //            else if(DBaseUse == DBaseType.dbNetCRMAU)
        //            {
        //                tpl_query = Regex.Replace(tpl_query, "LivMacola", "NetCRMAU", RegexOptions.IgnoreCase);
        //                tpl_query = Regex.Replace(tpl_query, "EXACTPH", "NetCRMAU", RegexOptions.IgnoreCase);
        //                tpl_query = Regex.Replace(tpl_query, "LIVEXACT2", "LivExactAus", RegexOptions.IgnoreCase);
        //                tpl_query = Regex.Replace(tpl_query, @"LIVEXACT3\\LIVEHK", "LivExactAus", RegexOptions.IgnoreCase);
        //                tpl_query = Regex.Replace(tpl_query, "NetCRMAU.WEB", "LivingWebRent.Web", RegexOptions.IgnoreCase);
        //                string[] OldVar = { "[NetCRMAU].WEB", "[NetCRMAU].[WEB]", "NetCRMAU.[WEB]" };
        //                tpl_query = MultipleReplace(tpl_query, OldVar, "[LivingWebRent].[Web]");
        //                string[] OldVar2 = { "FROM WEB", "FROM [WEB]" };
        //                tpl_query = MultipleReplace(tpl_query, OldVar2, "FROM [LivingWebRent].[Web]");
        //                string[] OldVar3 = { "UPDATE WEB", "UPDATE [WEB]" };
        //                tpl_query = MultipleReplace(tpl_query, OldVar3, "UPDATE [LivingWebRent].[Web]");
        //                string[] OldVar4 = { "INTO WEB", "INTO [WEB]" };
        //                tpl_query = MultipleReplace(tpl_query, OldVar4, "INTO [LivingWebRent].[Web]");
        //                string[] OldVar5 = { "JOIN WEB", "JOIN [WEB]" };
        //                tpl_query = MultipleReplace(tpl_query, OldVar5, "JOIN [LivingWebRent].[Web]");
        //            }
        //            else
        //            {
        //                MessageBox.Show("Database not specified.");
        //            }
        //            return tpl_query;
        //        }

        //        public SqlConnection GetSqlConnection()
        //        {
        //            return mCon;
        //        }

        //        public void Execute(string SQL)
        //        {
        //            SqlCommand cmd = mCon.CreateCommand();
        //            cmd.CommandText = SQL;
        //            cmd.CommandTimeout = 600000;
        //            cmd.ExecuteNonQuery();
        //            cmd.Dispose();
        //        }

        //        public DataSet Data(string SQL)
        //        {
        //            SqlDataAdapter da = new SqlDataAdapter(SQL, mCon);
        //            DataSet ds = new DataSet();
        //            da.Fill(ds);			
        //            return ds;			
        //        }
        //        public DataSet Data(string SQL, int Timeout)
        //        {
        //            SqlCommand cmd = new SqlCommand();
        //            DataSet ds = new DataSet();
        //            SqlDataAdapter da = new SqlDataAdapter();
        //            cmd.CommandText = SQL;
        //            cmd.Connection = mCon;
        //            cmd.CommandTimeout = Timeout;
        //            da.SelectCommand = cmd;
        //            da.Fill(ds);
        //            return ds;
        //        }

        //        #region Execute SQL

        //        public static SqlConnection GetCon(string source, ref bool isNeedClose)
        //        {
        //            SqlConnection con;
        //            switch (source.ToUpper())
        //            {
        //                case "NETCRM": con = DataSource.ConnNetCRMAU; break;
        //                case "LIVEXACT": con = DataSource.ConnLivExactAUS; break;
        //                case "UniChoice": con = DataSource.ConnWeb; break;
        //                case "WEB": con = DataSource.ConnWeb; break;
        //                case "LIVINGDATA": con = DataSource.getConn("Livingdate"); isNeedClose = true; break;
        //                case "LIVCRM": con = DataSource.getConn("Livingdate"); isNeedClose = true; break;
        //                default: con = DataSource.ConnNetCRMAU; break;
        //            }

        //            return con;

        //        }

        //        public static Boolean ExecuteSQL(string strdb,string sql)
        //        {
        //            ArrayList paramC = new ArrayList();
        //            return ExecuteSQL(strdb, sql, paramC, CommandType.Text);
        //        }

        //        public static Boolean ExecuteSQL(string strdb,string sql,CommandType ctype)
        //        {
        //            ArrayList paramC = new ArrayList();
        //            return ExecuteSQL(strdb,sql,paramC,ctype);
        //        }

        //        public static Boolean ExecuteSQL(string strdb,string sql,ArrayList  paramC)
        //        {
        //            return ExecuteSQL (strdb,sql,paramC,CommandType.Text);
        //        }


        //        public static Boolean ExecuteSQL(string strdb,string sql,ArrayList  paramC, CommandType ctype)
        //        {
        //            #region Database
        ////			Database db =new LivCrm (); 
        ////			if (strdb.ToUpper()=="LIVCRM")
        ////			{
        ////				db = new LivCrm();
        ////			}
        //            Database db = new NETCRM();
        //            if (strdb.ToUpper()=="NETCRM")
        //            {
        //                db = new NETCRM();
        //            }
        //            else if (strdb.ToUpper()=="LIVEXACT")
        //            {
        //                db = new LivExact();
        //            }
        ////			else if (strdb.ToUpper()=="LIVINGDATA")
        ////			{
        ////				db = new LivingData ();
        ////			}
        //            else if (strdb.ToUpper()=="WEB")
        //            {
        //                db = new WEB ();
        //            }

        //                #endregion

        //            if (db.currentConnection != null) db.currentConnection.isActive = false;
        //                SqlCommand com = db.GetSqlConnection().CreateCommand();
        //                com.CommandType = ctype;
        //                com.CommandText = sql;

        //                try
        //                {
        //                    com.ExecuteNonQuery();
        //                    return true;		
        //                }
        //                catch(Exception z)
        //                {
        //                    if (z.Message != sqlErrorException)
        //                    {
        //                        MessageBox.Show(z.Message, "NetCRM", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                        Errorlog(z.Message.ToString(), formname, "public static Boolean ExecuteSQL(string strdb,string sql,ArrayList  paramC, CommandType ctype) - " + z.TargetSite.ToString());
        //                    }
        //                    return false;		
        //                }
        //                finally
        //                {
        //                    com.Dispose();
        //                    if (db.currentConnection != null) db.currentConnection.isActive = true;
        //                    db.Dispose();
        //                }		

        //        }
        //        #endregion 

        //        #region GetValue
        //        public static string GetValue(string strdb,string sql, string strField)
        //        {


        //            ArrayList paramC= new ArrayList();
        //            return GetValue (strdb,sql,strField,paramC,CommandType.Text);
        //        }

        //        public static string GetValue(string strdb,string sql, string strField,CommandType ctype)
        //        {
        //            ArrayList paramC= new ArrayList();
        //            return GetValue (strdb,sql,strField,paramC,ctype);
        //        }
        //        public static string GetValue(string strdb,string sql, string strField,ArrayList paramC )
        //        {
        //            return GetValue (strdb,sql,strField,paramC,CommandType.Text);
        //        }

        //        public static string GetValue(string strdb,string sql, string strField, ArrayList paramC,CommandType ctype)
        //        {
        //            string strValue="";

        //            #region Database
        //            Database db;
        ////			if (strdb.ToUpper()=="LIVCRM")
        ////			{
        ////				db = new LivCrm();
        ////			}
        //            if (strdb.ToUpper()=="NETCRM")
        //            {
        //                db = new NETCRM();
        //            }
        //            else if (strdb.ToUpper()=="LIVEXACT")
        //            {
        //                db = new LivExact();
        //            }
        ////			else if (strdb.ToUpper()=="LIVINGDATA")
        ////			{
        ////				db = new LivingData ();
        ////			}
        //            else if (strdb.ToUpper()=="WEB")
        //            {
        //                db = new WEB ();
        //            }
        //            else 
        //            {
        //                MessageBox.Show ("database couldn't be located","NetCRM");
        //                return "";
        //            }
        //            #endregion

        //            if (db.currentConnection != null) db.currentConnection.isActive = false;
        //            SqlCommand com = db.GetSqlConnection().CreateCommand();
        //            SqlDataAdapter da = new SqlDataAdapter();						

        //            DataSet  dst = new DataSet();						
        //            try
        //            {
        //                com.CommandType = ctype;
        //                com.CommandText = sql;				

        //                da.SelectCommand = com;

        //                #region Parameters
        //                SqlParameter p;
        //                foreach (clsParameter param in paramC)
        //                {
        //                    p = com.Parameters.Add(param.Parameter,param.Type);
        //                    switch (param.Type)
        //                    {
        //                        case SqlDbType.Char :
        //                        {
        //                            p.Value = param.Value;
        //                            break;
        //                        }
        //                        case SqlDbType.VarChar  :
        //                        {
        //                            p.Value = param.Value;
        //                            break;
        //                        }

        //                        case SqlDbType.Bit :
        //                        {
        //                            p.Value = Convert.ToBoolean(Convert.ToInt16(param.Value));
        //                            break;
        //                        }
        //                    }


        //                }
        //                #endregion

        //                da.Fill(dst);

        //                if (dst.Tables[0].DefaultView.Count>0)
        //                {
        //                    try
        //                    {
        //                        strValue =dst.Tables[0].Rows[0][strField].ToString();
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        strValue ="";
        //                        Errorlog(e.Message.ToString(),formname,"public static string GetValue(string strdb,string sql, string strField, ArrayList paramC,CommandType ctype) - " + e.TargetSite.ToString());
        //                    }

        //                }


        //            }
        //            catch (Exception ex)
        //            {
        //                if (ex.Message != sqlErrorException)
        //                {
        //                    MessageBox.Show(ex.Message.ToString(), "NetCRM", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                    Errorlog(ex.Message.ToString(), formname, "public static string GetValue(string strdb,string sql, string strField, ArrayList paramC,CommandType ctype) - " + ex.TargetSite.ToString());
        //                }
        //            }
        //            finally
        //            {
        //                da.Dispose();
        //                com.Dispose();
        //                if (db.currentConnection != null) db.currentConnection.isActive = true;
        //                db.Dispose();			

        //            }

        //            return strValue;		
        //        }
        //        #endregion

        //        #region Get Datatable
        //        public static DataTable GetDataTable(string strdb,string sql)
        //        {
        //            ArrayList paramC = new ArrayList();
        //            return GetDataTable(strdb, sql, paramC, CommandType.Text);
        //        }

        //        public static DataTable GetDataTable(string strdb,string sql,CommandType ctype)
        //        {


        //            ArrayList paramC = new ArrayList();
        //            return GetDataTable(strdb, sql, paramC, CommandType.Text);
        //        }

        //        public static DataTable GetDataTable(string strdb,string sql,ArrayList  paramC)
        //        {
        //            return GetDataTable(strdb,sql,paramC,CommandType.Text);
        //        }

        //        public static DataTable  GetDataTable(string strdb,string sql, ArrayList paramC,CommandType ctype)
        //        {
        //            #region Database
        //            Database db =new NETCRM (); 
        ////			if (strdb.ToUpper()=="LIVCRM")
        ////			{
        ////				db = new LivCrm();
        ////			}
        //            if (strdb.ToUpper()=="NETCRM")
        //            {
        //                db = new NETCRM();
        //            }
        //            else if (strdb.ToUpper()=="LIVEXACT")
        //            {
        //                db = new LivExact();
        //            }
        //            else if (strdb.ToUpper()=="LIVINGDATA")
        //            {
        //                db = new LivingData ();
        //            }
        //            else if (strdb.ToUpper()=="WEB")
        //            {
        //                db = new WEB ();
        //            }

        //            #endregion
        //            if (db.currentConnection != null) db.currentConnection.isActive = false;
        //            SqlCommand com = db.GetSqlConnection().CreateCommand();

        //            #region Parameters
        //            SqlParameter p;
        //            foreach (clsParameter param in paramC)
        //            {
        //                p = com.Parameters.Add(param.Parameter,param.Type);
        //                switch (param.Type)
        //                {
        //                    case SqlDbType.Char :
        //                    {
        //                        p.Value = param.Value;
        //                        break;
        //                    }
        //                    case SqlDbType.VarChar  :
        //                    {
        //                        p.Value = param.Value;
        //                        break;
        //                    }

        //                    case SqlDbType.Bit :
        //                    {
        //                        p.Value = Convert.ToBoolean(Convert.ToInt16(param.Value));
        //                        break;
        //                    }
        //                }


        //            }
        //            #endregion

        //            SqlDataAdapter da = new SqlDataAdapter();						

        //            DataTable dt = new DataTable();						
        //            try
        //            {
        //                com.CommandTimeout = 60000;
        //                com.CommandType =ctype;
        //                com.CommandText = sql;				

        //                da.SelectCommand = com;
        //                da.Fill(dt);

        //            }
        //            catch (Exception ex)
        //            {
        //                if (ex.Message != sqlErrorException)
        //                {
        //                    MessageBox.Show(ex.Message.ToString(), "NetCRM", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                    Errorlog(ex.Message.ToString(), formname, "public static DataTable  GetDataTable(string strdb,string sql, ArrayList paramC,CommandType ctype) - " + ex.TargetSite.ToString());
        //                }
        //            }
        //            finally
        //            {
        //                da.Dispose();
        //                com.Dispose();
        //                if (db.currentConnection != null) db.currentConnection.isActive = true;
        //                db.Dispose();		

        //            }
        //            return dt;		
        //        }
        //        #endregion
        //    }

        //        public class LivingData : Database
        //        {
        ////			public LivingData() : base("User ID=dev;Initial Catalog=livingdata;Data Source=NetCRMAU;Pooling=false;Password=dev")
        ////			{}		
        //            #region Original
        ////            public LivingData() : base("User ID=dev;Initial Catalog=NetCRM;Data Source=%s;Pooling=false; Password=dev; Application Name=NetCRM")
        ////            {}		
        //            #endregion

        //            public LivingData() : base("User ID=dev;Initial Catalog=Livingdata;Data Source=Livingerp;Pooling=false; ")
        //            {}		

        //        }

        //        public class LivCrm : Database
        //        {
        //            //public LivCrm()	: base("User ID=dev;Initial Catalog=livcrm;Data Source=NetCRMAU;Pooling=false;Password=dev; Application Name=NetCRM")
        //            public LivCrm() : base("User ID=dev;Initial Catalog=NetCRM;Data Source=%s;Pooling=false; ")
        //            {}
        //        }

        //        public class NETCRM : Database
        //        {
        //            public  NETCRM(): base("User ID=dev;Initial Catalog=NetCRM;Data Source=%s;Pooling=false; ")
        //            {}
        //        }

        //        public class LivExact : Database
        //        {
        //            public LivExact() : base("User ID=dev;Initial Catalog=135;Data Source=%e;Pooling=false")
        //            {}		
        //        }

        //        public class WEB : Database
        //        {
        //            public WEB() : base("User ID=dev;Initial Catalog=WEB;Data Source=%w;Pooling=false")
        //            {}		
        //        }

        //        public class UniChoice : Database
        //        {
        //            //		public UniChoice() : base("User ID=sa;Initial Catalog=UniChoice;Data Source=LIVINGWEBRENT;Pooling=false")
        //            public UniChoice()
        //                : base("Server=livingwebrent;Database=unichoice;uid=dev;")
        //            { }
        //        }

        //    internal class MyDllConfig : IDisposable
        //    {
        //        private string _oldConfig;
        //        private bool _libCompat;
        //        private const string _newConfig = "mydll.dll.config";
        //        // don't forget to rename this!!

        //        internal MyDllConfig()
        //        {
        //            _libCompat = Assembly.GetAssembly(typeof(
        //              ConfigurationSettings)).GetName().Version.ToString().
        //              CompareTo("1.0.5000.0") == 0;
        //            _oldConfig = AppDomain.CurrentDomain.GetData(
        //                   "APP_CONFIG_FILE").ToString();
        //            Switch(_newConfig);
        //        }
        //        protected void Switch(string config)
        //        {
        //            if (_libCompat)
        //            {
        //                AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", config);
        //                FieldInfo fiInit = typeof(
        //                    System.Configuration.ConfigurationSettings).GetField(
        //                        "_configurationInitialized",
        //                        BindingFlags.NonPublic | BindingFlags.Static);
        //                FieldInfo fiSystem = typeof(
        //                    System.Configuration.ConfigurationSettings).GetField(
        //                        "_configSystem", BindingFlags.NonPublic | BindingFlags.Static);
        //                if (fiInit != null && fiSystem != null)
        //                {
        //                    fiInit.SetValue(null, false);
        //                    fiSystem.SetValue(null, null);
        //                }
        //            }
        //        }

        //        public void Dispose()
        //        {
        //            Switch(_oldConfig);
        //        }

        //        public static string ConnectionString()
        //        {
        //            string cstr;
        //            using (new MyDllConfig())
        //            {
        //                //cstr = ConfigurationSettings.AppSettings["ConnectionString"];
        //                cstr = ConfigurationManager.ConnectionStrings["ConnectionString"];
        //            }
        //            return cstr;
        //        }
    }
}
