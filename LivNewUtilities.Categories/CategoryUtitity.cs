﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using LivNewCore.Models;
using System.Data;

namespace LivNewUtilities.Categories
{
    public class CategoryUtitity
    {
        //public static Stack<string> path = new Stack<string>();
        public static void SaveAsJSONFile(string saveToDirectory, string cat_id, bool includeChild)
        {
            GetCategoriesAndSave(saveToDirectory, cat_id);
            if (includeChild)
            {
                List<Category> parentCategories = Category.GetWebsitePartialCategories(cat_id);
                if (parentCategories == null)
                    return;
                //path.Push(cat_id);
                //Console.WriteLine(string.Join(", ", path.ToArray()));
                foreach(Category category in parentCategories) {
                    GetCategoriesAndSave(saveToDirectory, category.cat_id);
                    RecursiveCategories(category, saveToDirectory);
                }
                //if(path.Count > 0)
                //    path.Pop();
            } 
        }

        private static void RecursiveCategories(Category category, string saveToDirectory)
        {
            List<Category> childrenCategories = Category.GetWebsitePartialCategories(category.cat_id);
            if (childrenCategories == null)
                return;
            //path.Push(category.cat_id);
            //Console.WriteLine(string.Join(", ", path.ToArray()));
            foreach (Category childCategory in childrenCategories)
            {
                GetCategoriesAndSave(saveToDirectory, childCategory.cat_id);
                RecursiveCategories(childCategory, saveToDirectory);
            }
            //if (path.Count > 0)
            //    path.Pop();
        }

        private static void GetCategoriesAndSave(string saveToDirectory, string cat_id)
        {
            //get child categories
            Category currentCategory = Category.GetCategoryById(cat_id);
            if (cat_id.TrimEnd() == "0")
                currentCategory = new Category() { cat_id = "0", cat_ref_id = "0", has_child = true, cat_name = "Livingstone", url = "/livingstone" };
            if (currentCategory == null)
            {
                throw new ArgumentException("Cat_id cannot be found. Please check if still exists.");
            }
            DataSet dataSet = Category.GetDataSet(cat_id);
            List<Category> categories = Category.GetFromDataSet(dataSet);
            List<Menu> menus = Menu.GetFromDataSet(dataSet);
            dataSet.Dispose();

            string jsonContent = JsonConvert.SerializeObject(
                new
                {
                    cat_id = currentCategory.cat_id,
                    cat_name = currentCategory.cat_name.TrimEnd(),
                    cat_ref_id = currentCategory.cat_ref_id == "0" ? currentCategory.cat_id : currentCategory.cat_ref_id,
                    categories = categories.Select(c => new { 
                        cat_id = c.cat_id,
                        cat_name = c.cat_name.TrimEnd(),
                        has_child = c.has_child,
                        url = c.url
                    }),
                    menus = menus.Select(a => new {
                        a.order,
                        a.caption,
                        a.url,
                        a.menu_class,
                        a.menu_type,
                        a.menu_group,
                        a.visible_on,
                        a.group_class,
                        a.caption_style,
                        a.open_newtab,
                        a.url_debug
                    })
                }
            );
            //save categories to FILE as JSON format
            string fileLocation = string.Format("{0}/{1}.json", saveToDirectory, currentCategory.cat_id);
            if (System.IO.File.Exists(fileLocation))
                System.IO.File.Delete(fileLocation);
            System.IO.File.WriteAllText(fileLocation, jsonContent);
            Console.WriteLine(cat_id + " saved.");
        }
    }
}
