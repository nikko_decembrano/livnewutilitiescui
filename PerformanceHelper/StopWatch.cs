﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PerformanceHelper
{
    public enum StopWatchUnit
    { 
        ms, sec, min
    }

    public class StopWatch : IDisposable
    {
        private DateTime _startDate;
        private DateTime _endDate;
        private string _taskName;
        private StopWatchUnit _unit;
        private Dictionary<StopWatchUnit, Action<string, DateTime, DateTime>> _actions;

        public StopWatch(string taskName, StopWatchUnit unit)
        {
            if (string.IsNullOrEmpty(taskName))
                taskName = string.Empty;
            if (unit == null)
                unit = StopWatchUnit.ms;

            SetupActions();
            _startDate = DateTime.Now;
            _taskName = taskName;
            _unit = unit;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0}: {1} started", _startDate, _taskName);
        }

        private void SetupActions()
        {
            _actions = new Dictionary<StopWatchUnit, Action<string, DateTime, DateTime>>();
            _actions.Add(StopWatchUnit.sec, 
                (s, ds, de) => {
                    TimeSpan ts = de.Subtract(ds);
                    if (string.IsNullOrEmpty(s))
                        Console.WriteLine("{1}\n\t: Total Execution Time: {0} sec.", ts.TotalSeconds, de);
                    else
                        Console.WriteLine("{2}\n\t: Total Execution Time for {0}\n\t: {1} sec.", s, ts.TotalSeconds, de);
                }
            );

            _actions.Add(StopWatchUnit.ms, 
                (s, ds, de) =>
                {
                    TimeSpan ts = de.Subtract(ds);
                    if (string.IsNullOrEmpty(s))
                        Console.WriteLine("{1}\n\t: Total Execution Time: {0} ms.", ts.TotalMilliseconds, de);
                    else
                        Console.WriteLine("{2}\n\t: Total Execution Time for {0}\n\t: {1} ms.", s, ts.TotalMilliseconds, de);
                }
            );

            _actions.Add(StopWatchUnit.min, 
                (s, ds, de) =>
                {
                    TimeSpan ts = de.Subtract(ds);
                    if (string.IsNullOrEmpty(s))
                        Console.WriteLine("{1}\n\t: Total Execution Time: {0} min.", ts.TotalMinutes, de);
                    else
                        Console.WriteLine("{2}\n\t: Total Execution Time for {0}\n\t: {1} min.", s, ts.TotalMinutes, de);
                }
            );
        }

        public void Dispose()
        {
            _endDate = DateTime.Now;
            TimeSpan elapsed = _endDate.Subtract(_startDate);
            Action<string, DateTime, DateTime> output = _actions[_unit];
            Console.ForegroundColor = ConsoleColor.Green;
            output(_taskName, _startDate, _endDate);
        }
    }
}
