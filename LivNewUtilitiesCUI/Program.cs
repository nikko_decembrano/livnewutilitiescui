﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LivNewUtilities.Categories;
using PerformanceHelper;

namespace LivNewUtilitiesCUI
{
    class Program
    {
        static void Main(string[] args)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            Console.Write("Enter cat_id: ");
            string cat_id = Console.ReadLine();
            using (StopWatch sw = new StopWatch("Save Categories to JSON File " + cat_id, StopWatchUnit.sec))
            {
                CategoryUtitity.SaveAsJSONFile(baseDirectory + @"\Categories", cat_id, true);
            }
            
            Console.Write("Press any key to exit");
            Console.ReadKey();
        }
    }
}
