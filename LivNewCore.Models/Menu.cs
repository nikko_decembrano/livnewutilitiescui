﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using LivNewCore.DAL;

namespace LivNewCore.Models
{
    public class Menu : IDisposable
    {
        public int order { get; set; }
        public string caption { get; set; }
        public string url { get; set; }
        public string menu_class { get; set; }
        public string menu_type { get; set; }
        public string menu_group { get; set; }
        public string visible_on { get; set; }
        public string group_class { get; set; }
        public string caption_style { get; set; }
        public bool open_newtab { get; set; }
        public string url_debug { get; set; }

        public static List<Menu> GetFromDataSet(DataSet dataSet)
        {
            List<Menu> data = new List<Menu>();

            if (dataSet != null
                && dataSet.Tables.Count > 0)
            {
                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    data.Add(FunctionDB.InitFromDataRow<Menu>(row));
                }
            }

            return data;
        }

        #region IDisposable Members

        public void Dispose()
        {
            order = 0;
            caption = null;
            url = null;
            menu_class = null;
            menu_type = null;
            menu_group = null;
            visible_on = null;
            group_class = null;
            caption_style = null;
            open_newtab = default(bool);
            url_debug = null;
        }

        #endregion
    }
}
