﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using LivNewCore.DAL;

namespace LivNewCore.Models
{
    public class Category : IDisposable
    {
        public string cat_id { get; set; }
        public string cat_ref_id { get; set; }
        public string cat_name { get; set; }
        public string cat_id_root { get; set; }
        public string url { get; set; }
        public bool public_view { get; set; }
        public int count_product { get; set; }
        public bool has_child { get; set; }

        #region IDisposable Members

        public void Dispose()
        {
            cat_id = null;
            cat_ref_id = null;
            cat_name = null;
            cat_id_root = null;
            url = null;
            count_product = 0;
        }

        #endregion

        public static List<Category> GetWebsitePartialCategories(string cat_id)
        {
            object[,] sqlParam = { { "@cat_id", cat_id } };
            string sql = "EXEC dbo.GetWebsitePartialCategories @cat_id";
            DataTable result = DataSource.GetTableFrom(sql, "unichoice", sqlParam);
            if (result != null && result.Rows.Count > 0)
            {
                List<Category> categories = new List<Category>();
                foreach (DataRow row in result.Rows)
                {
                    categories.Add(FunctionDB.InitFromDataRow<Category>(row));
                }
                return categories;
            }
            return null;
        }

        public static DataSet GetDataSet(string cat_id)
        {
            return DataSource.getDataSet(
                "EXEC dbo.GetWebsitePartialCategories @cat_id",
                "unichoice",
                new string[] { "Categories", "Menus" },
                new object[,] {{"@cat_id", cat_id}}
            );
        }

        public static Category GetCategoryById(string cat_id)
        {
            Category xret = null;

            object[,] xarr_param = {
               {"@cat_id",cat_id}
            };

            DataTable xdt = DataSource.GetTableFrom(@"
                    SELECT
	                    ISNULL(tb2.cat_id_parent, tb1.cat_ref_id) as cat_id_root,
	                    tb1.cat_id as cat_id, 
	                    tb1.cat_name as cat_name,
	                    tb1.cat_ref_id as cat_ref_id,
	                    tb1.public_view as public_view
                    FROM catparchi_sql as tb1
                    LEFT JOIN cat_relation tb2
                    ON tb2.Cat_ID=tb1.cat_id
                    AND tb2.is_root=1
                    WHERE tb1.cat_id = @cat_id
                    ORDER BY tb1.cat_name
                ",
                 "UniChoice",
                 xarr_param
             );

            if (xdt.Rows.Count > 0)
            {
                xret = FunctionDB.InitFromDataRow<Category>(xdt.Rows[0]);
                if (xret.cat_id_root == "")
                {
                    xret = null;
                }
            }

            return xret;
        }

        public static List<Category> GetFromDataSet(DataSet dataSet)
        {
            List<Category> data = new List<Category>();

            if (dataSet != null
                && dataSet.Tables.Count > 0)
            {
                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    data.Add(FunctionDB.InitFromDataRow<Category>(row));
                }
            }

            return data;
        }
    }
}
